# CKA

Here's some points for people you may want to take the CKA certificate.

## What you need?
- Basic understanding of container runtime (Docker/Podman or whatever)
- Kubernetes understanding. You may learn it the hardway by following the [official documentation](https://kubernetes.io/docs/home/) and this is also the official documentation you can refer during taking exam.
- Or you can choose the easy way: [
Certified Kubernetes Administrator (CKA) with Practice Tests](https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/learn/lecture/14224074#overview)

## What I did?
- Doing the easy way above
- Taking all of pratise tests 2 - 3 times, including the Moc Exam
- Refer [K8s the hardway](https://github.com/kelseyhightower/kubernetes-the-hard-way) 1 time

## Note for the exam: 
- You should read the [candidate handbook carefully](https://docs.linuxfoundation.org/tc-docs/certification/lf-candidate-handbook), below is just some of them.
- New version of CKA exam has 15 - 20 tasks (mine is 17 tasks), you'll have 120mins for them. ~7mins for each task.
- Easy task have 4% or 5%, harder has 7%. The most hardest task has 13%. So you should concentrate on the easy task first, flag the harder task if it take too much time, then came back later.
- 66% is passing score, you'll have 1 free retake.
- When you schedule your exam slot, TLF will allow you access the "test exam" session. There's 2 session with same tasks. Those tsaks are harder than the real exam. If you can solve all of tasks on time, you can pass the real exam easily.
- During the exam, you'll be requested to close all of other applications on your PC, just remains only the browser (Chrome is recommended). On your browser, only 2 tabs is allow: 1 for the exam terminal, 1 for the documentation.
- https://discuss.kubernetes.io/ is not allow anymore in during the exam. Glad to say, you will not need it ;) 

## My referrer:
- [Thao Luong](https://medium.com/@luongvinhthao/how-to-get-the-cka-from-beginner-6923c5f2c546) - My ex-colleague
- [Vinh Nguyen](https://github.com/vinhnguyen116/CKA-CKAD) - My ex-colleague

## My certificate:

![](CKA-Cert-NamPham.png)
